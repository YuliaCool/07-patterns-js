export const users = new Map(); // map of connected users, values template: {username, actualRoom}

export default io => {
  io.on("connection", socket => {
    
    socket.on('login', () => {
      const username = socket.handshake.query.username;
    
      if (users.has(username.toLowerCase())) {
        console.log("User with this login has already in game! " + username);
        socket.emit('user-error', { message: "User with this login has already in game!" });
      }
      else {
        users.set(username.toLowerCase(), null);
        socket.emit('start-game');
      }
    });

    socket.on("logout", () => {
      const username = socket.handshake.query.username;
      console.log(`${username} logged out`);
      users.delete(username);
    });

    socket.on('disconnect', () => { 
      // this event caling when user close tab, update page and goes from users namespace to rooms namespace
      console.log('user disconnect from user namespace');
  });
  });
};
