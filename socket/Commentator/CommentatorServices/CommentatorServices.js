import { Comment } from '../CommentsClasses/Comment';
import { room } from '../currentRoomProxy';

export class CommentatorServises {
    constructor() {
    }
    get roomName() {
        return this._roomName;
    }
    set roomName(roomName) {
        this._roomName = roomName;
    }
    get commentText() {
        return this._commentText;
    }
    set commentText(commentText) {
        this._commentText = commentText;
    }

    get commentColor() {
        return this._commentColor;
    }
    set commentColor(commentColor) {
        this._commentColor = commentColor;
    }

    sendComment(){
        return this.commentText;
    }
    sendColorComment() {
        return this.commentColor;
    }

    addRoom(value) {
        this._roomName = value;
    }

    startGameAtDay(){
        return new Comment('greeting', this._roomName, 'дня');
    }
    startGameAtEvening(){
        return new Comment('greeting', this._roomName, 'вечора');
    }
    startGame(userNamesList){
        let usersNames = "";
        userNamesList.forEach(user => {
            usersNames += ` ${user}`;
        });
        return new Comment('info', this._roomName, `Давайте починати гру! На позиціях ${usersNames}`);
    }

    userLeaveRoom(userName){
        return new Comment('info', this._roomName, `Охрана, отмена! (${userName} передумав грати та вийшов із кімнати)`);
    }
    userDisconnect(userName){
        return new Comment('info', this._roomName, `${userName} покинув нас назавжди (вилогінився)`);
    }

    gameOver(users, currentUser) {
        return new Comment('infoAboutWinner', this._roomName, { users, currentUser });
    }

    userGoingForward(user){
        return new Comment('infoAboutPlayerStatus', this._roomName,  { user } );
    }

    userFinished(user) {
        return new Comment('infoAboutPlayerStatus', this._roomName, { user });
    }
    sendJoke() {
        return new Comment('joke', this._roomName);
    }

    sayGoodBye(){
        return new Comment('cheers', this._roomName, 'дня');
    }
}

