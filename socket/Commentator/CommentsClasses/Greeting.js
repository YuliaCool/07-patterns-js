export class Greeting {
    constructor(properties) {
        this._dayPart = properties;
        this.color = '#20639b80';
        this.text = `Гарного ${this._dayPart} шановні клавогонці! Сьогодні я буду радий коментувати із Вами ці цікаві перегони. Вьйо до клавіатур!`;
    }
}