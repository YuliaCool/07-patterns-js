export class InfoWinner {
    constructor(properties) {
        this.usersOnFinish = properties.users;
        this.winner = properties.currentUser;
        this.color = '#F6D55C';
        this.text = `Та переможцем нашего чемпіонату стає та отримує нову клавіатуру - ${this.winner}, за ним йдуть всі інші ${this.usersOnFinish}`;
    }
}