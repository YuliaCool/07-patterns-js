// Factory pattern creates different types of comments
import { Cheers } from './Cheers';
import { Greeting } from './Greeting';
import { InfoStatus } from './InfoStatus';
import { InfoWinner } from './InfoWinner';
import { InfoComment } from './InfoComment';
import { Joke } from './Joke';

export class Comment {
    constructor(type, roomName, properties) {
        this._roomName = roomName;

        if(type === 'joke')
            return new Joke();

        if(type === 'greeting') {
            return new Greeting(properties);
        }

        if(type === 'infoAboutPlayerStatus')
            return new InfoStatus(properties);

        if(type === 'infoAboutWinner')
            return new InfoWinner(properties);

        if(type === 'cheers')
            return new Cheers(properties);

        if(type === 'info')
            return new InfoComment(properties);
    }
}









