export class Cheers {
    constructor(properties) {
        this._dayPart = properties.dayPart;
        this.color = '#ED553B';
        this.text = `Нехай нещастя та негоди не роблять Вам в житті погоди! Гарного ${this._dayPart} та на все добре!`;
    }
}