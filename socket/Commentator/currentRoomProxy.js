import { CommentatorServises } from './CommentatorServices/CommentatorServices';

// object for using proxy. With this objct we will easy use proxy item then with rooms collection
export const currentRoom = { roomName: '', users: [], currentUser: '', gameIsStarted: false, gameIsCompleted: false, status: '' };
export let bot = new CommentatorServises();

const handler = {

    set(obj, prop, value) {
        if ((prop === 'roomName') && (value != '')) {
            bot.addRoom(value);
            return Reflect.set(...arguments);
        }
        if (prop === 'openRoom') {
            if (value === true) { 
                let comment = bot.startGameAtDay();
                bot.commentText = comment.text;
                bot.commentColor = comment.color;
                return Reflect.set(...arguments);
            }
            else return Reflect.set(...arguments);
        }
        if (prop === 'gameIsStarted') {
            if (value === true) {
                let comment = bot.startGame(currentRoomProxy.users);
                bot.commentText = comment.text;
                bot.commentColor = comment.color;
                return Reflect.set(...arguments);
            }
            else return Reflect.set(...arguments);
        }
        if (prop === 'gameIsCompleted') {
            if (value === true) {
                let comment = bot.sayGoodBye();
                bot.commentText = comment.text;
                bot.commentColor = comment.color;
                return Reflect.set(...arguments);
            }
            else return Reflect.set(...arguments);
        }
        if (prop === 'users') {
            return Reflect.set(...arguments);
        }
        if (prop === 'currentUser') {
            return Reflect.set(...arguments);
        }
        if ((prop === 'status') && (value === 'leave')){
            const comment = bot.userLeaveRoom(currentRoomProxy.currentUser);
            bot.commentText = comment.text;
            bot.commentColor = comment.color;
            return Reflect.set(...arguments);
        }
        if ((prop === 'status') && (value === 'disconnect')){
            const comment = bot.userDisconnect(currentRoomProxy.currentUser);
            bot.commentText = comment.text;
            bot.commentColor = comment.color;
            return Reflect.set(...arguments);
        }
        if ((prop === 'status') && (value === "user is going forward")){
            const comment = bot.userGoingForward(currentRoomProxy.currentUser);
            bot.commentText = comment.text;
            bot.commentColor = comment.color;
            return Reflect.set(...arguments);
        }
        if ((prop === 'status') && (value === "finish")){
            const comment = bot.userFinished(currentRoomProxy.currentUser);
            bot.commentText = comment.text;
            bot.commentColor = comment.color;
            return Reflect.set(...arguments); 
        }
    }
};

// Proxy pattern for checking change situation in currentRoom object
export let currentRoomProxy = new Proxy(currentRoom, handler);


