import { rooms } from './rooms'; //delete roomName from this obj, make set not map
import { users } from './users';
import { SocketParent } from './socketParent';
import { SocketGameProcess } from './socketGameProcess';
import { checkReadyStartTimer } from './socketService';

export class SocketGameRooms extends SocketParent {
    constructor(io, socket) {
        super(io, socket);
    }

    updateRoomUsersList(roomName) {
        const room = rooms.get(roomName);
        this._io.to(roomName).emit('update-users-in-room', room);
    }

    updateReadyStatus() {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);
        const room = rooms.get(roomName);

        const usersInRoom = room.users;
        usersInRoom.forEach(user => {
            if(user.username == username) 
                user.readyStatus = !user.readyStatus;
        });
        let updatedRoom = { roomName, users: usersInRoom };

        let allUsersIsReady = checkReadyStartTimer(updatedRoom);

        if (allUsersIsReady) {
            const socketGameProcess = new SocketGameProcess(this._io, this._socket);
            socketGameProcess.startGame(updatedRoom);
        }
        else {
            updatedRoom.timer = false;
            rooms.set(roomName, updatedRoom);

            this._io.to(roomName).emit('update-users-in-room', updatedRoom);
        }
        
    }
};
