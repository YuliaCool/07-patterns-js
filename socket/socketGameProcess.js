import { rooms } from './rooms'; //delete roomName from this obj, make set not map
import { users } from './users';
import { SocketParent } from './socketParent';
import { SocketGameRoom } from './socketGameRoom';
import { getTextIdForGame, checkReadyStartTimer } from './socketService';
import { bot } from './Commentator/currentRoomProxy';

import { currentRoomProxy, currentRoom } from './Commentator/currentRoomProxy';

export class SocketGameProcess extends SocketParent {
    constructor(io, socket) {
        super(io, socket);
    }

    startGame(room) {
        room.timer = true;
        rooms.set(room.roomName, room);

        currentRoomProxy.gameIsStarted = true;
        this._io.to(room.roomName).emit('add-comment', bot.sendComment(), bot.sendColorComment());

        this._io.to(room.roomName).emit('update-users-in-room', room);
        this._io.to(room.roomName).emit('run-start-timer', getTextIdForGame());

        this._socket.emit("display-available-rooms", Array.from(rooms.values()));
    }

   updateUserProgressBar(persentTyped) {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);
        const room = rooms.get(roomName);

        const usersInRoom = room.users;
        usersInRoom.forEach(user => {
            if(user.username == username) {
                user.progressBarWidth = persentTyped;

                currentRoomProxy.status = "user is going forward";
                currentRoom.currentUser = username;
                this._io.to(roomName).emit('add-comment', bot.sendComment(), bot.sendColorComment());
            }
        });
        this._io.in(roomName).emit('update-progress-bar', usersInRoom);
    }

    updateFinishedUser(countTypedError) {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);
        const room = rooms.get(roomName);
        const usersInRoom = room.users;
        let countFinishedUsers = 0;

        usersInRoom.forEach(user => {
            if (user.completedStatus) ++countFinishedUsers;
        });

        usersInRoom.forEach(user => {
            if (user.username == username) {
                user.completedStatus = true;
                user.countErrors = countTypedError;
                user.place = ++countFinishedUsers;
                this._io.in(roomName).emit('update-progress-bar-to-finished-state', usersInRoom);

                //currentRoomProxy.users.push({ usename: user.username, place: user.place });
                currentRoomProxy.status = 'finish';
                currentRoomProxy.currentUser = user.username;
                this._io.to(roomName).emit('add-comment', bot.sendComment(), bot.sendColorComment());
            }
        });

        if(usersInRoom.length === countFinishedUsers) {
            this._io.in(roomName).emit('show-game-result', usersInRoom);

            //currentRoomProxy.gameIsCompleted = true;
            //this._io.to(roomName).emit('add-comment', bot.sendComment(), bot.sendColorComment());
        }   
    }

    completeGame() {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);

        currentRoomProxy.gameIsCompleted = true;
        this._io.to(room.roomName).emit('add-comment', bot.sendComment(), bot.sendColorComment());

        const room = rooms.get(roomName);
        const usersInRoom = room.users;
        usersInRoom.forEach(user => {
            user.readyStatus = false;
            user.completedStatus = false;
            user.countErrors = 0;
            user.place = 0;
        })
    }

}