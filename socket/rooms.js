import { SocketRooms } from './socketRooms';
import { SocketGameRooms } from './socketGameRoom';
import { SocketGameProcess } from './socketGameProcess';
import { Comment } from './Commentator/CommentsClasses/Comment';

export const rooms = new Map();

export default io => {
    io.on("connection", socket => {
        socket.emit("display-available-rooms", Array.from(rooms.values()));
        const socketRooms = new SocketRooms(io, socket);
        const commentator = new Comment(io, socket);

        socket.on('create-room', roomName => socketRooms.createNewRoom(roomName));
        socket.on('join-room', roomName => socketRooms.addUserToRoom(roomName));
        socket.on('leave-room', () => socketRooms.leaveCurrentRoom());

        const socketGameRoom = new SocketGameRooms(io, socket);

        socket.on('update-users-in-room', roomName => {
            socketGameRoom.updateRoomUsersList(roomName);
            commentator.sendComment(roomName);
        });
        socket.on('update-ready-status', () => socketGameRoom.updateReadyStatus());

        const socketGameProcess = new SocketGameProcess(io, socket);
        socket.on('update-progress', persentTyped => socketGameProcess.updateUserProgressBar(persentTyped));

        socket.on('winner', countTypedError => socketGameProcess.updateFinishedUser(countTypedError));

        socket.on('complete-game', () => socketGameProcess.completeGame());

        socket.on('disconnect', () => socketRooms.disconnect());
    });
};