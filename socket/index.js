import users from "./users";
import rooms from './rooms';

export default io => {
  users(io.of("/users"));
  rooms(io.of('/rooms'));
}