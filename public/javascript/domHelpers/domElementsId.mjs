export const roomsListContainerId = "rooms-list";
export const logoutButtonId = 'logout';
export const userLoginId = 'username-info';
export const createRoomButtonId = 'create-room';
export  const roomsPageContainerId = "rooms-page";
export const gamePageContainerId = "game-page";

export const playersListContainerId = "players-list";
export const buttonBackRoomListId = 'button-back-rooms';

export const timerId = 'timer';
export const textGameContainerId = 'textGame';
export const buttonBackRoomsId = 'button-back-rooms';
export const buttonReadyId = 'button-ready';
export const paragraphTextId = 'text';
export const textGameId = 'textGame';
export const coloredTextId = 'coloredText';
export const underlinedTextId = 'underlinedText';
export const gameTimerValueId = 'timerValue';
export const gameTimerId = 'game-timer';

export const comentatorContainerId = 'massages-container';