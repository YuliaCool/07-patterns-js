import { createElement, addClass, removeClass } from '../domHelpers/domHelper.mjs';

export const renderRoom = (room, joinRoomHandler) => {
    const roomPreviewContainer = createElement({tagName: 'div', className: 'room-preview-container', attributes: { id: room.roomName}});

    const usersInfoContainer = createElement({tagName: 'div'});
    const usersCountId = room.roomName+"users";
    const usersCount = createElement({tagName: 'span', className: 'room-info', attributes: { id: usersCountId }});
    usersCount.innerHTML = room.users.length;
    usersInfoContainer.append(usersCount);
    
    const roomInfoContainer = createElement({tagName: 'div', className: 'roomInfoContainer'});
    const roomName = createElement({ tagName: 'span', className: 'room-name'});
    const joinButton = createElement({ tagName: 'input', className: 'join-room-button', attributes: { type: 'button', value:'Join'} });
    joinButton.onclick = () => { joinRoomHandler(room.roomName); }
    roomName.innerHTML = room.roomName;
    roomInfoContainer.append(roomName);
    roomInfoContainer.append(joinButton);
    
    roomPreviewContainer.append(usersInfoContainer);
    roomPreviewContainer.append(roomInfoContainer);

    return roomPreviewContainer;
};

export const renderPlayer = (user) => {
    const playerContainer = createElement({
                                    tagName: 'div', 
                                    className: 'player-container'
                                });
    const playerHead = createElement({
                                    tagName: 'div', 
                                    className: 'player-header'
                                });
    const playerFlag = createElement({
                                    tagName: 'div', 
                                    className: 'player-flag',
                                    attributes: { id: `player-${user}-flag`}
                                });
    const playerName = createElement({
                                    tagName: 'div', 
                                    className: 'player-name',
                                    attributes: { id: `player-${user}-name`}
                                });
    playerName.innerHTML = user;
    const statusBar = createElement({
                                    tagName: 'div', 
                                    className: 'status-bar',
                                    attributes: { id: `player-${user}-statusbar`}
                                }); 
    const statusBarIndicator = createElement({
                                    tagName: 'div', 
                                    className: 'status-bar-indicator',
                                    attributes: { id: `player-${user}-statusbar-indicator`}
                                });                                                   
    playerHead.append(playerFlag);
    playerHead.append(playerName); 

    statusBar.append(statusBarIndicator);

    playerContainer.append(playerHead);
    playerContainer.append(statusBar);
    
    return playerContainer;
}

export const clearRootContainer = (idRoot) => {
    const rootDiv = document.getElementById(idRoot);
    while(rootDiv.firstChild){
      rootDiv.removeChild(rootDiv.firstChild);
    }
}

export const setClassById = (idElement, className) => {
    addClass(document.getElementById(idElement), className);
}
export const hideElement = idElement => {
    setClassById(idElement, 'display-none');
}
export const showElement = idElement => {
    removeClass(document.getElementById(idElement), 'display-none');
}