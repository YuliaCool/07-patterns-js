import {  hideElement, showElement, clearRootContainer } from '../javascript/domHelpers/domServise.mjs';
import * as domId from '../javascript/domHelpers/domElementsId.mjs';
import { SocketRoomService } from '../javascript/socketClientClasses/socketRoomService.mjs';
import { SocketGameProcessServices } from '../javascript/socketClientClasses/socketGameProcessServices.mjs';

const username = sessionStorage.getItem("username");
const socket = io("/rooms", { query: { username } }); 
if (!username) {
  window.location.replace("/login");
}

socket.on('room-error', (error) => {
  alert(error.message);
});

let countOfComment = 0;
const maxCountOfComment = 4;

const socketRooms = new SocketRoomService(socket, username);
socket.on('open-room', room => socketRooms.openRoom(room));
socket.on('display-available-rooms', rooms => socketRooms.displayAvailableRoom(rooms, onJoinRoom));
socket.on('add-room-on-page', room => socketRooms.addRoomOnPage(room));
socket.on('update-users-in-room', room => socketRooms.updateUsersInRoom(room));

const socketGame = new SocketGameProcessServices(socket, username);
socket.on('update-progress-bar', usersList => socketGame.updateProgressBar(usersList));
socket.on('run-start-timer', idText => socketGame.runStartTimer(idText));
socket.on('update-progress-bar-to-finished-state', usersInRoom => socketGame.updateProgressBarToCompleted(usersInRoom));
socket.on('show-game-result', usersList => socketGame.showGameResult(usersList));

socket.on('add-comment', (comment, commentColor) => {
  const newComment = `<div class ="commentator-comments" style="background-color:${commentColor};">
                                   <h4>Anton Petrovich: </h4>
                                   <h5>${comment}</h5>
                               </div>`;
  if(countOfComment <= maxCountOfComment) {
    document.getElementById(domId.comentatorContainerId).innerHTML += newComment;
  }
  else {
    countOfComment = 0;
    document.getElementById(domId.comentatorContainerId).innerHTML = newComment;
  }
  countOfComment ++;
});

window.onload = function() {
  document.getElementById(domId.logoutButtonId).onclick = logoutHandler;
  document.getElementById(domId.userLoginId).innerHTML = `Hi, ${sessionStorage.getItem("username")}!`;
  document.getElementById(domId.createRoomButtonId).onclick = createRoom;
  
  document.getElementById(domId.buttonReadyId).onclick = readyButtonHandler;
  document.getElementById(domId.buttonBackRoomListId).onclick = leaveRoom;
};

const logoutHandler = () => {
  const username = sessionStorage.getItem("username");
  const socket = io("/users", { query: { username } });
  socket.emit('logout');

  sessionStorage.removeItem('username');
  document.getElementById('username-info').innerHTML = "";
  window.location.replace("/login");
};

const createRoom = () => {
  const roomName = prompt('Enter room name');
  if (roomName)
    socket.emit('create-room', roomName);
};

const readyButtonHandler = () => {
  socket.emit('update-ready-status');

  const buttonText = (document.getElementById(domId.buttonReadyId).value == 'Ready') ? 'Not ready' : 'Ready';
  document.getElementById(domId.buttonReadyId).value = buttonText;
};

const leaveRoom = () => {
  hideElement(domId.gamePageContainerId);
  showElement(domId.roomsPageContainerId);

  socket.emit('leave-room');
};

const onJoinRoom = (roomName) => {
  socket.emit('join-room', roomName);
};

